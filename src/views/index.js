import Intro from './Intro.svelte'
import Turn from './Turn.svelte'
import Result from './Result.svelte'
export default {
    Intro,
    Turn,
    Result
}