import { writable, derived, get } from 'svelte/store'
export const turnTime = 3 * 60 * 1000 // 3 minutes
export const view = writable('Intro')

export const gameList = writable([
  {
    site: 'rbc',
    news: [
      { title: 'Турция сняла эмбарго на поставки поясов шахидов в Палестину Fake ', fake: true },
      { title: 'Первая встреча Лаврова и Блинкена, ФСБ и смартфоны. Главное за ночь' },
      { title: 'СМИ узнали о жалобах на работу информцентра ЦБ о хакерских атаках' },
      { title: 'В ЦБ описали новый дизайн сторублевой купюры' },
      { title: 'Глава владельца TikTok уйдет с поста директора' },
      { title: 'Интересующихся COVID россиян оказалось вдвое больше следящих за нефтью' },
      { title: 'СМИ сообщили о возвращении пошлины на ввоз электромобилей в Россию' },
      { title: 'Дуров заявил о погружении в Средневековье при использовании iPhone' },
      { title: 'СМИ сообщили о решении США ввести санкции против «Академика Черского»' },
      { title: 'Европарламент призвал Турцию признать геноцид армян' },
    ],
  },
  {
    site: 'rbc',
    news: [
      { title: 'Суд во Франции отказал кредиторам Березовского в праве на его поместье' },
      { title: 'В честь премьеры фильма «Форсаж-9» в Москве пройдут ночные гонки на каршеринговых автомобилях', fake: true },
      { title: 'Суд обязал Bellingcat выплатить ₽340 тыс. бывшему командиру ополчения ДНР' },
      { title: 'Как Airbnb нашел новый источник дохода в мире без туризма' },
      { title: '€215 млн заплатит «Лента» за покупку сети супермаркетов «Билла»'}
    ],
  },
])

export const activeIndex = writable(0)

export const activeNews = derived([activeIndex, gameList], ([$activeIndex, $gameList]) => {
  return $gameList[$activeIndex]
})

export const timer = (() => {
  const { update, subscribe } = writable(turnTime)
  let timeoutId
  const updateTime = () => {
    update((state) => {
      if (state <= 0) {
        clearTimeout(timeoutId)
        view.set('Result')
        return 0
      }
      return (state -= 10)
    })
    timeoutId = setTimeout(updateTime, 10)
  }
  return {
    subscribe,
    start: () => {
      clearTimeout(timeoutId)
      update(() => turnTime)
      updateTime()
    },
    stop: () => {
      clearTimeout(timeoutId)
    },
    wrongAnswer: () => update((state) => (state -= 10000)),
  }
})()

export const startTurn = () => {
  activeIndex.set(0)
  view.set('Turn')
}

export const rightAnswer = () => {
  if (get(activeIndex) === get(gameList).length - 1) {
    timer.stop()
    view.set('Result')
  } else {
    activeIndex.update((state) => state + 1)
  }
}
